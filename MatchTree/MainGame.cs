﻿using MatchTree.Data;
using MatchTree.GameField;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;


namespace MatchTree
{

    public class MainGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<Texture2D> texture;
        Field field;
        SpriteFont mousePosition;
        MouseState mouseState;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 256;
            graphics.PreferredBackBufferWidth = 500;
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            this.IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            mousePosition = Content.Load<SpriteFont>("MousePosition");
            texture = new List<Texture2D>();
            spriteBatch = new SpriteBatch(GraphicsDevice);
            foreach (int element in Enum.GetValues(typeof(ElementSquares)))
            {
                ElementSquares el = (ElementSquares)element;
                texture.Add(Content.Load<Texture2D>(el.GetDescription()));
            }
            field = new Field();
            field.FillField();

        }

        protected override void UnloadContent()
        {

        }


        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            mouseState = Mouse.GetState();
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            Vector2 possition = Vector2.Zero;
            spriteBatch.Begin();
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    spriteBatch.Draw(texture[(int)field.GetFieldFalue(x, y)], possition, Color.White);
                    possition.Y += 32;
                }
                possition.Y = 0;
                possition.X += 32;
            }
            spriteBatch.DrawString(mousePosition, $"x={mouseState.Position.X};y={mouseState.Position.Y}",new Vector2(300,10),Color.Black);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
