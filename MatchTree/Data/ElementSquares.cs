﻿using System.ComponentModel;

namespace MatchTree.Data
{
    public enum ElementSquares
    {
        [Description("element_blue_square")]
        ElementBlueSquare,

        [Description("element_green_square")]
        ElementGreenSquare,

        [Description("element_grey_square")]
        ElementGreySquare,

        [Description("element_purple_square")]
        ElementPurpleSquare,

        [Description("element_red_square")]
        ElementReadSquare,

        [Description("element_yellow_square")]
        ElementYellowSquare
    }
}