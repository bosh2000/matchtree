﻿using MatchTree.Data;
using System;

namespace MatchTree.GameField
{
    public class Field
    {
        public ElementSquares[,] field;

        public Field()
        {
            field = new ElementSquares[8, 8];
        }

        public ElementSquares GetFieldFalue(int x, int y)
        {
            return field[x, y];
        }

        public void FillField()
        {
            Random rnd = new Random();
            for (int x=0;x<7;x++)
                for (int y = 0; y < 7; y++)
                {
                    field[x, y] = (ElementSquares)rnd.Next(5);
                }
        }
    }
}